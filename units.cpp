/*
Output: Enter a number in meters:
Input: 600
Output:
600m=0.6km
600m=6000dm
600m=60000cm*/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
  int m = 0;
  cout <<  "Enter a number in meters:";
  cin >> m;
  cout << m << "m =" << m / 1000. << "km" << endl;
  cout << m << "dm =" << m*10 << "dm";
  
  return 0;
 
}